#include<stdio.h>

//nested loop : level 2
int main(void)
{
	int i, j;
	int m = 8, n = 7;
	
	puts("Block 1");
	for (i = 0 ;i < m; ++i)
	{
		for (j = 0; j < m; ++j)
		{
			printf("i = %d , j = %d \n ", i, j);
		}
	}

	puts("Block 2");
	for (i = 0 ;i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if(i<j)	
				printf("i = %d , j = %d \n ", i, j);
		}
	}

	puts("Block 3");
	for (i = 0 ;i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if (i <= j)
			{
				printf("i = %d , j = %d \n ", i, j);

			}
		}
	}

	puts("Block 4");
	for (i = 0; i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if (i > j)
			{
				printf("i = %d , j = %d \n ", i, j);

			}
		}
	}

	puts("Block 5");
	for (i = 0; i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if (i >= j)
			{
				printf("i = %d , j = %d \n ", i, j);

			}
		}
	}

	puts("Block 6");
	for (i = 0 ;i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if (i == j)
			{
				printf("i = %d , j = %d \n ", i, j);

			}
		}
	}


	puts("Block 7");
	for (i = 0; i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if (i != j)
			{
				printf("i = %d , j = %d \n ", i, j);

			}
		}
	}
	return(0);

}


