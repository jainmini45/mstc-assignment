#include<stdio.h>
//looping right shift

int main(void)
{
	int i;
	int k;
	int n;

	puts("Block 1");
	for (i = 256;i > 0;i = i >> 2)
		printf("i =%d\n", i);

	puts("Block 2");
	printf("Enter N :");
	scanf("%d", &n);

	printf("Enter K :");
	scanf("%d", &k);

	for (i = n; i > 0;i >>= k)
		printf("i =%d\n", i);

	return (0);

}

